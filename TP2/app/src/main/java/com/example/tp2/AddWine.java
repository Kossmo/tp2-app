package com.example.tp2;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class AddWine extends AppCompatActivity {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_wine);

        setValue();
    }

    private void setValue(){
        final TextView add_nom = findViewById(R.id.wineName);
        final TextView add_region = findViewById(R.id.editWineRegion);
        final TextView add_loc = findViewById(R.id.editLoc);
        final TextView add_clim = findViewById(R.id.editClimate);
        final TextView add_surf = findViewById(R.id.editPlantedArea);
        Button button = findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nom = String.valueOf(add_nom.getText());
                String region = String.valueOf(add_region.getText());
                String loc = String.valueOf(add_loc.getText());
                String climat = String.valueOf(add_clim.getText());
                String surface = String.valueOf(add_surf.getText());
                if (nom.length() != 0 && region.length() != 0){
                    Wine nWine = new Wine(nom,region,loc,climat,surface);
                    WineDbHelper WDH = new WineDbHelper(AddWine.this);
                    boolean test = WDH.addWine(nWine);
                    if (test != false){
                        WineListAdapter.get(getApplicationContext(),WDH).getListe().add(nWine);
                        WineListAdapter.get(getApplicationContext(),WDH).notifyDataSetChanged();
                        finish();
                    }
                } else {
                    AlertDialog.Builder popup = new AlertDialog.Builder(AddWine.this);
                    popup.setTitle("Sauvegarde Impossible");
                    popup.setMessage("L'appelation et la région  doivent être remplis");
                    popup.show();
                }
            }
        });
    }

}

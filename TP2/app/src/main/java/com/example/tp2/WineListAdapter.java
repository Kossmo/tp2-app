package com.example.tp2;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import static com.example.tp2.WineDbHelper.cursorToWine;

public class WineListAdapter extends RecyclerView.Adapter<WineListAdapter.ViewHolder> {

    private static final String TAG = "WineListAdapter";
    private Context aContext;
    private static ArrayList<Wine> wineL = new ArrayList<>();
    private WineDbHelper WineDB;
    private static WineListAdapter singleton;

    public WineListAdapter(Context context, WineDbHelper WDB){
        aContext = context;
        WineDB = WDB;
        Cursor cursor = WDB.fetchAllWines();
        while(!cursor.isAfterLast()) {
            wineL.add(cursorToWine(cursor));
            cursor.moveToNext();
        }
    }

    public static  WineListAdapter get(Context context, WineDbHelper WDB){
        if(singleton == null) {
            singleton = new WineListAdapter(context,WDB);
        }
        return singleton;
    }

    public static ArrayList<Wine> getListe(){
        return wineL;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.reycler_wine, parent,false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
        }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        holder.vin.setText(wineL.get(position).getTitle());
        holder.lieu.setText(wineL.get(position).getRegion());
        holder.pLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(aContext,WineDescri.class);
                intent.putExtra("wine", wineL.get(position));
                intent.putExtra("position", position);
                aContext.startActivity(intent);
            }
        });
        holder.pLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                PopupMenu popup = new PopupMenu(aContext, holder.pLayout);
                popup.inflate(R.menu.supp_menu);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.supp:
                                WineDB.deleteWine(wineL.get(position));
                                wineL.remove(wineL.get(position));
                                notifyDataSetChanged();
                                break;
                        }
                        return false;
                    }
                });
                popup.show();
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
            return wineL.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView vin;
        TextView lieu;
        RelativeLayout pLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            vin = itemView.findViewById(R.id.vin);
            lieu = itemView.findViewById(R.id.lieu);
            pLayout = itemView.findViewById(R.id.pLayout);
        }
    }
}

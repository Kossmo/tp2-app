package com.example.tp2;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class WineDescri extends AppCompatActivity {

    private static final String TAG = WineDescri.class.getSimpleName();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine);

        getIncomingIntent();
    }

    private void getIncomingIntent() {
        if (getIntent().hasExtra("wine") && getIntent().hasExtra("position")) {
            Wine wine = (Wine) getIntent().getExtras().getParcelable("wine");
            Long id = wine.getId();
            String nom = wine.getTitle();
            String region = wine.getRegion();
            String loc = wine.getLocalization();
            String climat = wine.getClimate();
            String surface = wine.getPlantedArea();
            int position = getIntent().getIntExtra("position",0);
            setValue(id, nom, region, loc, climat, surface, position);
        }
    }

    private void setValue(final Long id, final String nom, final String region, String loc, String climat, String surface, final int position){
        final TextView descri_nom = findViewById(R.id.wineName);
        descri_nom.setText(nom);
        final TextView descri_region = findViewById(R.id.editWineRegion);
        descri_region.setText(region);
        final TextView descri_loc = findViewById(R.id.editLoc);
        descri_loc.setText(loc);
        final TextView descri_clim = findViewById(R.id.editClimate);
        descri_clim.setText(climat);
        final TextView descri_surf = findViewById(R.id.editPlantedArea);
        descri_surf.setText(surface);

        Button button = findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nom2 = String.valueOf(descri_nom.getText());
                String region2 = String.valueOf(descri_region.getText());
                String loc2 = String.valueOf(descri_loc.getText());
                String climat2 = String.valueOf(descri_clim.getText());
                String surface2 = String.valueOf(descri_surf.getText());
                if (nom2.length() != 0 && region2.length() != 0){
                    Wine nWine = new Wine(id, nom2,region2,loc2,climat2,surface2);
                    WineDbHelper WDH = new WineDbHelper(WineDescri.this);
                    WDH.updateWine(nWine);
                    WineListAdapter.getListe().get(position).setRegion(region2);
                    WineListAdapter.getListe().get(position).setLocalization(loc2);
                    WineListAdapter.getListe().get(position).setClimate(climat2);
                    WineListAdapter.getListe().get(position).setPlantedArea(surface2);
                    WineListAdapter.get(getApplicationContext(),WDH).notifyDataSetChanged();
                } else {
                    AlertDialog.Builder popup = new AlertDialog.Builder(WineDescri.this);
                    popup.setTitle("Sauvegarde Impossible");
                    popup.setMessage("L'appelation et la région  doivent être remplis");
                    popup.show();
                }
            }
        });
    }

}
